import { Template } from 'meteor/templating';
import { Tareas } from '../api/tareas.js'; 

import './tarea.js';
import './body.html';
 

Template.body.helpers({
  tareas() {
    return Tareas.find({}, { sort: { createdAt: -1 }});
  },
});

Template.body.events({

  'submit .nueva-tarea'(event) {
    // Prevent default browser form submit
    event.preventDefault();
 
    // Get value from form element
    const target = event.target;
    const texto = target.texto.value;
 
    // Insert a task into the collection
    Tareas.insert({
      texto:texto,
      createdAt: new Date(), // current time
    });
 
    // Clear form
    target.texto.value = '';
  },
});